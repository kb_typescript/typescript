(() => {
    
    // Tipado del retorno de una Funcion
    const sumar = (a: number, b:number): number => a + b;

    const nombre = ():String => 'HOla Elvis';


    const obtenerSalario = (): Promise<string> => {
        return new Promise( ( resolve, reject ) => {
            resolve('1');

        }); 
    }


    obtenerSalario().then( a => console.log( a.toUpperCase() ) );

})();