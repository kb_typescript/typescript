(() => {
    
    interface Xmen {
        nombre: string;
        edad: number;
        poder?: string;
    }

    const enviarMision = (xmen:Xmen ) => {
        console.log(`Enviando a ${ xmen.nombre } a la mision`);
    }

    const regresarAlCuartel = (xmen:Xmen ) => {
        console.log(`Enviando a ${ xmen.nombre } al cuarte`);
    }

    const volverine: Xmen = {
        nombre: 'Login',
        edad: 60
    }

    enviarMision( volverine );
    regresarAlCuartel( volverine );
    


})();
