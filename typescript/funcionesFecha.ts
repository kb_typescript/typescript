(function(){
    
    // funciona tradicional asignada a una variable let
    let miFuncion = function( a: string){
        return a.toUpperCase();
    }

    // funcion con Flecha
    const miFuncionF = ( a: String ) => a.toUpperCase();


    // ========

    const sumarN = function(a: number, b: number) {
        return a + b;
    }

    const sumarNFecha = (a: number, b: number) => a + b;


    // ========

    const hulk = {
        nombre: 'Hulk',
        smash(){

            setTimeout( () => {
                console.log(` ${ this.nombre } smash!! `);
            }, 1000 );           
        }
    }

    hulk.smash();




})();

