(() => {
    
    const retirarDinero = ( montoRetirar: number ): Promise<number> => {
        
        let dineroActual = 500;   
                
        return new Promise( ( resolve, reject ) => {
            if ( montoRetirar > dineroActual ) {
                reject ('No hay suficiente fondos');
            } else {
                dineroActual -= montoRetirar;
                resolve( dineroActual );
            }
        });    
    }

    retirarDinero ( 1000 )
        .then( montoActual => console.log( `Me queda ${ montoActual }` ) )
        .catch( console.warn );

})();
